import React, { Component, Fragment } from "react";
import { Segment, Button, Input, ButtonGroup } from "semantic-ui-react";
import firebase from "../../firebase";

class MessageForm extends Component {
  state = {
    message: "",
    channel: this.props.currentChannel,
    user: this.props.currentUser,
    loading: false,
    errors: [],
  };

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  createMessage = () => {
    const { user } = this.state;
    const message = {
      timestamp: firebase.database.ServerValue.TIMESTAMP,
      user: {
        id: user.uid,
        name: user.displayName,
        avatar: user.photoURL,
      },
      content: this.state.message,
    };
    return message;
  };

  sendMessage = () => {
    const { messagesRef } = this.props;
    const { message, channel } = this.state;

    if (message) {
      this.setState({ loading: true });
      messagesRef
        .child(channel.id)
        .push()
        .set(this.createMessage())
        .then(() => {
          this.setState({
            loading: false,
            message: "",
            errors: [],
          });
        })
        .catch((err) => {
          console.error(err);
          this.setState({
            loading: false,
            errors: this.state.errors.concat(err),
          });
        });
    } else {
      this.setState({
        errros: this.state.errors.concat({ message: "Add a message" }),
      });
    }
  };

  render() {
    const { errors, message, loading } = this.state;
    return (
      <Fragment>
        <Segment className="message__form">
          <Input
            fluid
            name="message"
            onChange={this.handleChange}
            style={{ marginBottom: "0.7em" }}
            label={<Button icon={"add"} />}
            labelPosition="left"
            value={message}
            className={
              errors.some((error) => error.includes("message ")) ? "error" : ""
            }
            placeholder="Write your message"
          />
          <ButtonGroup icon widths="2">
            <Button
              onClick={this.sendMessage}
              color="orange"
              content="Add Reply"
              disabled={loading}
              labelPosition="left"
              icon="edit"
            />
            <Button
              color="teal"
              content="Upload Media"
              labelPosition="right"
              icon="cloud upload"
            />
          </ButtonGroup>
        </Segment>
      </Fragment>
    );
  }
}

export default MessageForm;
